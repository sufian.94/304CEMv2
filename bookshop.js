
'use strict'

const request = require('./modules/request')
const google = require('./modules/google')
const persistence = require('./modules/persistence')

/**
 * Searches for books
 *
 * @function search
 * @param req the request object passed by the API
 * @param {Function(Error, String)} callback run on completion
 */
exports.search = (req, callback) => {
	let query = undefined
	try {
		query = request.getParameter(req, 'q')
	} catch(err) {
		callback(err)
	}
	google.searchByString(query, (err, data) => {
		if (err) {
			callback(err)
			return
		}
		const clean = request.replaceHostname(req, data)
		callback(null, clean)
	})
}

/**
 * Returns a book with the matching ISBN number
 *
 * @function getBook
 * @param req the request object passed by the API
 * @param {Function(Error, String)} callback run on completion
 */
exports.getBook = (req, callback) => {
	let isbn = undefined
	try {
		isbn = request.getParameter(req, 'isbn')
	} catch(err) {
		callback(err)
	}
	google.getByISBN(isbn, (err, data) => {
		if(err) {
			callback(err)
			return
		}
		callback(null, data)
	})
}

/**
 * Adds a new user.
 *
 * @function addUser
 * @param req the request object passed by the API
 * @param {Function(Error, String)} callback run on completion
 */
exports.addUser = (req, callback) => {
	console.log('addUser')
	let credentials
	try {
		credentials = request.getCredentials(req)
		if (request.getHeader(req, 'content-type') !== 'application/json') {
			throw new Error('content-type header must be application/json')
		}
	} catch(err) {
		callback(err)
		return
	}
	persistence.validUsername(credentials, (err, data) => {
		if (err) {
			console.log('validUsername error')
			callback(err)
			return
		}
		console.log(data)
		if (data === true) {
			callback(new Error('username already exists'))
			return
		}
		console.log()
		const name = request.extractBodyKey(req, 'name')
		const user = {name: name}
		console.log(user)
		persistence.addAccount(credentials, user, (err, data) => {
			if (err) {
				callback(err)
				return
			}
			callback(null, data)
			return
		})
	})
}

/**
 * Adds a book to the shooping cart associated with a valid user.
 *
 * @function addToCart
 * @param req the request object passed by the API
 * @param {Function(Error, String)} callback run on completion
 */
exports.addToCart = (req, callback) => {
	let credentials, isbn
	try {
		credentials = request.getCredentials(req)
		isbn = request.extractBodyKey(req, 'isbn')
	} catch(err) {
		callback(err)
		return
	}
	persistence.validCredentials(credentials, (err, data) => {
		if (err) {
			callback(err)
			return
		}
		if (data === false) {
			callback(new Error('invalid credentials'))
			return
		}
		console.log('A')
		google.getByISBN(isbn, (err, data) => {
			console.log('B')
			if (err) {
				console.log('C')
				callback(err)
				return
			}
			console.log('we have a valid account and a valid ISBN')
			persistence.addBook(credentials, data, (err, data) => {
				console.log('E')
				if (err) {
					console.log('F')
					callback(err)
					return
				}
				callback(null, data)
			})
		})
	})
}

exports.getCart = (req, callback) => {
	let credentials
	try {
		credentials = request.getCredentials(req)
	} catch(err) {
		callback(err)
		return
	}
	persistence.validCredentials(credentials, (err, data) => {
		if (err) {
			callback(err)
			return
		}
		if (data === false) {
			callback(new Error('invalid credentials'))
			return
		}
		console.log('valid credentials')
		persistence.getBooksInCart(credentials, (err, data) => {
			if (err) {
				console.log('A')
				console.log(err.message)
				callback(err)
				return
			}

			callback(null, {cart: data})
		})
	})
}
