
'use strict'

const rewire = require('rewire')

const request = rewire('../modules/request')
const json = require('./data/request.json')

describe('Google Module', () => {

	describe('getParameter', () => {
		it('extract q parameter from valid data', () => {
			try {
				const data = request.getParameter(json.good, 'q')
				expect(data).toEqual('nodejs')
			} catch(err) {
				expect(true).toBe(false) // no error should be thrown
			}
		})

		it('throw an error when trying to extract a missing parameter', () => {
			try {
				request.getParameter(json.bad, 'q')
				expect(true).toBe(false) // error should be thrown
			} catch(err) {
				expect(err.message).toEqual('q parameter missing')
			}
		})
	})

	describe('extractBodyKey', () => {

		it('extract isbn key from request body', () => {
			try {
				const data = request.extractBodyKey(json.good, 'isbn')
				expect(data).toEqual('53335')
			} catch(err) {
				expect(true).toBe(false) // no error should be thrown
			}
		})

		it('throw an error when trying to extract a missing key', () => {
			try {
				request.extractBodyKey(json.bad, 'isbn')
				expect(true).toBe(false) // error should be thrown
			} catch(err) {
				expect(err.message).toEqual('missing key isbn in request body')
			}
		})

	})

	describe('getCredentials', () => {

		it('should correctly extract credentials when supplied', () => {
			try {
				const data = request.getCredentials(json.good)
				expect(data.username).toBe('johndoe')
				expect(data.password).toBe('p455w0rd')
			} catch(err) {
				expect(true).toBe(false) // no error thrown
			}
		})

		it('should throw error if auth key missing', () => {
			try {
				request.getCredentials(json.missingAuth)
				expect(true).toBe(false) // no error thrown
			} catch(err) {
				expect(err.message).toBe('authorization header missing')
			}
		})

		it('should throw error if basic key missing', () => {
			try {
				request.getCredentials(json.missingBasic)
				expect(true).toBe(false) // no error thrown
			} catch(err) {
				expect(err.message).toBe('authorization header missing')
			}
		})

		it('should throw error if username missing', () => {
			try {
				request.getCredentials(json.missingUsername)
				expect(true).toBe(false) // no error thrown
			} catch(err) {
				expect(err.message).toBe('missing username / password')
			}
		})

		it('should throw error if password missing', () => {
			try {
				request.getCredentials(json.missingPassword)
				expect(true).toBe(false) // no error thrown
			} catch(err) {
				expect(err.message).toBe('missing username / password')
			}
		})

	})

	describe('replaceHostname', () => {
		it('should detect hostname when supplied', () => {
			try {
				const data = request.replaceHostname(json.good, json.books)
				expect(data.books.length).toEqual(2)
				expect(data.books[0].link).toContain('http://www.example.com')
				expect(data.books[1].link).toContain('http://www.example.com')
			} catch(err) {
				expect(true).toBe(false) // no error thrown
			}
		})

		it('should use default hostname when not supplied', () => {
			try {
				const data = request.replaceHostname(json.bad, json.books)
				expect(data.books.length).toEqual(2)
				expect(data.books[0].link).toContain('http://localhost')
				expect(data.books[1].link).toContain('http://localhost')
			} catch(err) {
				expect(true).toBe(false) // no error thrown
			}
		})
	})

})
